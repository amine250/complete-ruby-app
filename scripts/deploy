#!/bin/sh

set -e

if [[ $# -ne 3 ]]; then
  echo "usage: $0 <app-name> <app-image> <app-domain>"
  exit 1
fi

app_name="$1"
app_image="$2"
app_domain="$3"

echo "Storing Kubernetes CA..."
kubectl config set-cluster gitlab-deploy --server="$KUBE_URL" \
  --certificate-authority="$KUBE_CA_PEM_FILE"

echo "Storing Kubernetes Credentials..."
kubectl config set-credentials gitlab-deploy --token="$KUBE_TOKEN"

echo "Defining and using a new Kubernetes configuration..."
kubectl config set-context gitlab-deploy \
  --cluster=gitlab-deploy --user=gitlab-deploy \
  --namespace="$KUBE_NAMESPACE"
kubectl config use-context gitlab-deploy

echo "Creating namespace..."
cat <<EOF | kubectl apply -f -
kind: Namespace
apiVersion: v1
metadata:
  name: $KUBE_NAMESPACE
EOF

echo "Creating deployment to run application pods..."
cat <<EOF | kubectl apply --force -n $KUBE_NAMESPACE -f -
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: $app_name
  labels:
    app: $app_name
    job_id: "$CI_JOB_ID"
spec:
  replicas: 1
  template:
    metadata:
      labels:
        name: $app_name
        app: $app_name
        job_id: "$CI_JOB_ID"
    spec:
      containers:
      - name: app
        image: $app_image
        ports:
        - name: web
          containerPort: 5000
EOF

echo "Creating service to expose 5000 of application pods..."
cat <<EOF | kubectl apply --force -n $KUBE_NAMESPACE -f -
apiVersion: v1
kind: Service
metadata:
  name: $app_name
  labels:
    app: $app_name
    job_id: "$CI_JOB_ID"
spec:
  ports:
    - name: web
      port: 5000
      targetPort: web
  selector:
    name: $app_name
EOF

echo "Creating ingress to expose application service to outside world under $app_domain..."
cat <<EOF | kubectl apply --force -n $KUBE_NAMESPACE -f -
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: $app_name
  labels:
    app: $app_name
    job_id: "$CI_JOB_ID"
spec:
  rules:
  - host: $app_domain
    http:
      paths:
      - path: /
        backend:
          serviceName: $app_name
          servicePort: 5000
EOF

echo "Waiting for deployment..."
kubectl rollout status -n "$KUBE_NAMESPACE" -w "deployment/$app_name"
